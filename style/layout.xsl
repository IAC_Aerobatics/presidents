<?xml version="1.0" encoding="UTF-8"?>
	<!--
		Stylesheet takes care of page layout settings. It ought to provide a
		default box size. It must provide three named templates, all called
		from the root node. - head-content provides content inside the head
		tag - body-attributes provides attribute content for the body element
		- body-content provides markup content for the body element The third
		template, body-content may contaian an <apply-templates/> to process
		source document elements below the root; otherwise, the stylesheet
		will process no such elements.
	-->
<xsl:stylesheet version='1.0'
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- 
<xsl:import href="menuTemplate.xsl"/>
-->
	<xsl:import href="footerTemplate.xsl" />

	<!-- default box size for images -->
	<xsl:param name="box-size">
		400
	</xsl:param>

	<!--
		add content to the html head element. the calling template takes care
		of the title. link any stylesheets here.
	-->
	<xsl:template name="head-content">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="en-us" />
		<meta name="rating" content="General" />
		<meta name="revisit-after" content="7 days" />
		<meta name="ROBOTS" content="ALL" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<link href="/style/site.css" type="text/css" rel="stylesheet" />
		<link rel="shortcut icon" href="/favicon.gif" type="image/gif" />
		<link rel="stylesheet" type="text/css" href="style/site.css" />
	</xsl:template>

	<xsl:template name="body-attributes" />

	<xsl:template name="body-content">
		<div style="z-index: 0; position: absolute; left: 0px; top: 0px;">
			<img src="images/RescBanner.jpg" />
		</div>
		<div style="z-index: 1; position: relative; top: 0px; left: 0px">
			<img src="images/BannerIAC.png" alt="IAC logo" />
		</div>
		<xsl:if test="/node()[@title!='']">
			<div
				style="position: absolute; top: 75px; left: 162px; z-index: 1; color: black; vertical-align: bottom; font-family: Times, serif; font-size: 36pt;">
				<xsl:value-of select="/node()/@title" />
			</div>
			<div
				style="position: absolute; top: 73px; left: 160px; z-index: 2; color: white; vertical-align: bottom; font-family: Times, serif; font-size: 36pt;">
				<xsl:value-of select="/node()/@title" />
			</div>
		</xsl:if>
		<div class="content">
			<!-- 
			<xsl:call-template name="menu-template" />
			 -->
			<xsl:apply-templates />
		</div>
		<xsl:call-template name="footer-template" />
	</xsl:template>

</xsl:stylesheet>
